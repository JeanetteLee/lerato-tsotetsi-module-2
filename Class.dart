void main () {
  MtnWinner winner = new MtnWinner();
  winner.app_name = 'Rekindle Learning app';
  winner.category = 'Best Women in STEM';
  winner.developer = 'Rapelang Rabana';
  winner.year = 2021;
  String capital = winner.capital_letters(winner.app_name);

  String message =
  'In ${winner.year}, the app ${winner.app_name} of the developer ${winner.developer} was announced as the winner in the category of ${winner.category}';
  print(message);

}
class MtnWinner {
  late String app_name;
  late String category;
  late String developer;
  late int year;

  String capital_letters(capital){
    app_name = app_name.toUpperCase();
        return app_name;
    }
  }